#ifndef WEB_SCRAPING_CURL_FUNCTIONS_H
#define WEB_SCRAPING_CURL_FUNCTIONS_H

#include <string>
#include <iostream>
#include <curl/curl.h>

std::size_t WriteCallback(char *contents, std::size_t size, std::size_t nmemb, std::string *buffer);
std::string sendGETRequest(const std::string& url);


#endif //WEB_SCRAPING_CURL_FUNCTIONS_H
