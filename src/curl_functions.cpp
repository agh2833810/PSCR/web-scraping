#include "curl_functions.h"

std::size_t WriteCallback(char *contents, std::size_t size, std::size_t nmemb, std::string *buffer) {
    std::size_t real_size = size * nmemb;
    buffer->append(contents, real_size);
    return real_size;
}

std::string sendGETRequest(const std::string& url) {
    std::string response_buffer;
    CURL *curl = curl_easy_init();
    CURLcode res;

    if(curl){
        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &response_buffer);

        res = curl_easy_perform(curl);

        if(res != CURLE_OK) {
            std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << std::endl;
        }

        curl_easy_cleanup(curl);
    }
    return response_buffer;
}
