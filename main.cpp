#include <iostream>
#include <thread>
#include <semaphore>
#include <exception>
#include <cstdlib>
#include "curl_functions.h"
#include "send_functions.h"
#include "json_processing.h"

static const char* host_env = std::getenv("WEB_SCRAPING_HOST");

void retrieveDataThread(std::binary_semaphore *semaphore, const std::string &url, std::unique_ptr<std::string> *data){
    semaphore->acquire();
    std::string raw_data = sendGETRequest(url);
    JSONParser::remove_whitespaces(raw_data);
    *data = std::make_unique<std::string>(raw_data);
    semaphore->release();
}

void sendDataThread(std::binary_semaphore *semaphore, SendMessages *send_api, std::unique_ptr<std::string> *data){
    semaphore->acquire();
    JSONParser json(*(*data));
    object_type new_json{};
    new_json.emplace("timestamp", json["timestamp"]);
    JSONValue productions = json["data"]["podsumowanie"];
    new_json.emplace("produkcja_wiatr", productions["wiatrowe"]);
    new_json.emplace("produkcja_wodna", productions["wodne"]);
    new_json.emplace("produkcja_fotowoltaika", productions["PV"]);
    new_json.emplace("produkcja_cala", productions["generacja"]);
    new_json.emplace("zapotrzebowanie", productions["zapotrzebowanie"]);
    new_json.emplace("produkcja_cieplne", productions["cieplne"]);
    new_json.emplace("czestotliwosc", productions["czestotliwosc"]);

    JSONValue json_object(new_json);
    std::string msg = json_object.as_str();
    try {send_api->send_message(msg);}
    catch (const SendException &err) {
        if(err.get_error_id() == 0 || err.get_error_id() == 1){
            std::cout << "Failed to establish connection, message: " << err.what() << std::endl;
        }
        if(err.get_error_id() == 2){
            std::cout << "Failed to send, abolished connection, message: " << err.what() << std::endl;
        }
    }
    semaphore->release();
}

void run_job(SendMessages &api, std::binary_semaphore &main_semaphore){
    std::unique_ptr<std::string> data;
    std::thread th1(retrieveDataThread, &main_semaphore, "https://www.pse.pl/transmissionMapService", &data);
    while(main_semaphore.try_acquire()) main_semaphore.release();
    std::thread th2(sendDataThread, &main_semaphore, &api, &data);
    th1.join(); th2.join();
}

int main() {
    if(host_env == nullptr){
        std::cout << "Host variable not set, defaulting to 0.0.0.0" << std::endl;
        host_env = "0.0.0.0";
    }
    else std::cout << "Host value: " << host_env << std::endl;

    SendMessages send_api(10000, host_env); // establish connection to database on given port, at given IP
    try{send_api.establish_connection();}
    catch (const SendException &err){
        std::cout << "Failed to connect at startup, message: " << err.what() << std::endl;
    }
    std::binary_semaphore main_semaphore{0};
    main_semaphore.release();
    int i=0;
    while(i<10000){ // set to 4 bc killing process dont close connection properly, default should be 'true'
        ++i;
        try {
            std::cout << "Started operation" << std::endl;
            auto startTime = std::chrono::steady_clock::now();
            auto nextTime = startTime + std::chrono::seconds(15);
            run_job(send_api, main_semaphore);
            std::this_thread::sleep_until(nextTime);
        }
        catch (const std::exception& e){
            std::cout << e.what() << std::endl;
        }
    }
    return EXIT_SUCCESS;
}
