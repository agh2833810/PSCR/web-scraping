import socket


if __name__ == '__main__':
    print("Started")
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.bind(("0.0.0.0", 10000))
    while True:
        server_socket.listen(1)
        client_socket, client_address = server_socket.accept()
        while True:
            data = client_socket.recv(1024)
            if not data:
                break
            print(f"Received data: {data.decode()}")
